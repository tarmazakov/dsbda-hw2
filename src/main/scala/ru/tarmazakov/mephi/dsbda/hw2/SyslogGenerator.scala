package ru.tarmazakov.mephi.dsbda.hw2

import java.util.Random

import scala.sys.process.Process

class SyslogGenerator() {
  var log_level = Map[String, String](
    "0" -> "emerg",
    "1" -> "alert",
    "2" -> "crit",
    "3" -> "err",
    "4" -> "warning",
    "5" -> "notice",
    "6" -> "info",
    "7" -> "debug"
  )

  def run(timeOutInSeconds: Integer): Unit = {
    val random = new Random()
    val runUntil = System.currentTimeMillis() + (timeOutInSeconds * 1000)
    while (System.currentTimeMillis() < runUntil) {
      val level = log_level.get(random.nextInt(7).toString)
      val command = "logger -p local7." + level.get + " message"
      println(command)
      Process(command).!
      Thread.sleep(10)
    }
  }
}
