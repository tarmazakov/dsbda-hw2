package ru.tarmazakov.mephi.dsbda.hw2

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions._
import org.apache.spark.{Logging, SparkConf, SparkContext}


/**
  * The application that subtracts the number of syslog events per hour
  **/
object SyslogCountByHour extends Object with Logging {

  /**
    * Main method
    *
    * @param args args[0] - path in hdfs to the files
    *             args[1] - time in seconds, for generating syslog events
    */
  def main(args: Array[String]) {
    val directory = args(0)
    val timeOutSec = Integer.parseInt(args(1))

    //run generate logs
    logInfo("Generate random syslog")
    val syslogGenerator = new SyslogGenerator()
    syslogGenerator.run(timeOutSec)
    logInfo("SyslogGenerator starting successfully")


    val hdfsFileSystem: FileSystem = initHDFSFileSystem


    val filePaths = hdfsFileSystem.listStatus(new Path(directory))
      .map(fileStatus => fileStatus.getPath.toString)
      .filter(file => !file.contains("tmp"))
      .toList
    filePaths.foreach(x => println(x))

    val sqlContext: SQLContext = initSQLContext

    val df = sqlContext.read.format("com.databricks.spark.csv")
      .option("header", "false")
      .option("inferSchema", "true")
      .load(filePaths.mkString(","))
      .toDF(Seq("timestamp", "hostname", "facility", "severity", "message"): _*)

    var df2: DataFrame = calculateResult(df)

    df2.printSchema()
    df2.show()

    df2.withColumn("severity", when(col("severity").equalTo("0"), "emerg")).show()
  }

  def calculateResult(df: DataFrame) = {
    var df2 = df
      .withColumn("date", from_unixtime(col("timestamp") / 1000, "yyyy-MM-dd"))
      .withColumn("hour", from_unixtime(col("timestamp") / 1000, "h a"))
      .groupBy("date", "hour", "severity")
      .count()
    df2
  }

  /**
    * Returns the configured filesystem implementation.
    *
    * @return FileSystem
    */
  private def initHDFSFileSystem = {
    val configuration = new Configuration()
    configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"))
    configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"))
    val hdfsFileSystem = FileSystem.get(configuration)
    hdfsFileSystem
  }

  /**
    * Method for initializing SparkContext
    *
    * @return new instance of SparkContext
    */
  private def initSQLContext = {
    val sc = new SparkContext(new SparkConf()
      .setAppName("Spark count syslog events by hour")
      .setMaster("local[2]")
      .set("spark.executor.memory", "2g"))
    new SQLContext(sc)
  }
}