import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.Before;
import org.junit.Test;
import ru.tarmazakov.mephi.dsbda.hw2.SyslogCountByHour;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SyslogCounterTest {

    private DataFrame testDF;

    @Before
    public void setup() {
        SparkConf conf = new SparkConf().setAppName("SparkSample").setMaster("local");
        JavaSparkContext jsc = new JavaSparkContext(conf);
        SQLContext sqc = new SQLContext(jsc);
        // sample data
        List<String> data = new ArrayList<>();
        data.add("1512199204902, 1");
        data.add("1512199204902, 1");
        data.add("1512199204902, 2");
        // DataFrame
        DataFrame df = sqc.createDataset(data, Encoders.STRING()).toDF();
        DataFrame df1 = df.selectExpr("split(value, ',')[0] as timestampp", "split(value, ',')[1] as severity");
        testDF = df1.selectExpr("cast(timestampp as long) timestamp", "severity");
        testDF.show();
    }

    @Test
    public void calculateResult() {

        DataFrame result = SyslogCountByHour.calculateResult(testDF);
        result.printSchema();
        result.show();

        //ASSERTS

        System.out.println(result.col("date")); //thows exceptions if not exist
        System.out.println(result.col("hour"));
        System.out.println(result.col("severity"));
        System.out.println(result.col("count"));

        Row[] dataRows = result.collect();
        assertEquals(dataRows.length, 2);
        assertEquals(dataRows[0].toString(), "[2017-12-02,10 AM, 1,2]");
        assertEquals(dataRows[1].toString(), "[2017-12-02,10 AM, 2,1]");

    }

}
