#!/bin/bash

start(){
    ./hw2-flume-hdfs-sparksql/bin/hw2-flume-hdfs-sparksql hdfs://quickstart.cloudera:8020//user/cloudera/flume/syslog/ 20
}

init(){
    sudo usermod -a -G vboxsf cloudera

    hadoop fs -mkdir /user/cloudera/flume
    hadoop fs -mkdir /user/cloudera/flume/syslog

    cp flume.conf /etc/flume-ng/conf/

    sudo cp adaltas-flume-0.0.1-SNAPSHOT.jar /usr/lib/flume-ng/lib/
    sudo chmod +r /usr/lib/flume-ng/lib/adaltas-flume-0.0.1-SNAPSHOT.jar
}

case "$1" in
	(start)
	start
	exit 0;;

	(init)
	init
	exit 0;;
	(*)
	echo "Usage: $0 {
	    init - init environment for flume->hdfs->spark-sql application
	    start <work directory> - start application.
	}
	"
	exit 2;;
esac